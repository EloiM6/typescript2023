// Atenció!!!! QUE HI HAGI EL ".JS"
import { Pokemon } from "./classe_Pokemon.js";
import { Evolucio } from "./Evolucio.js";
import { Evolucio2 } from "./Evolucio2.js";
import { PokemonInterfaceClasse } from "./Classe_Pokemon_Interficie.js";
// Exemple de instancia d'un objecte
let Bulbasaur = new Pokemon("Bulbasaur", 1, 100, true);
Bulbasaur.toString();
Bulbasaur.HP = 1000;
Bulbasaur.toString();
// Exemple d'instancia d'una herencia
let Ivysaur = new Evolucio("Ivysaur", 2, 120, true);
Ivysaur.toString();
Ivysaur.HP = 1200;
Ivysaur.NomEvolucio = "Venusaur";
Ivysaur.toString();
// Exemple d'instancia d'una herencia amb sobrescriptura del constructor i dels mètodes
let Venusaur = new Evolucio2("Venasaur", 3, 200, true, "");
Venusaur.toString();
Venusaur.HP = 2000;
Venusaur.NomEvolucio = "Venusaur VMax";
Venusaur.toString();
// Mostra com se sobrescriu l'atribut readonly del pare
console.log("Sobreescriptura de l'atribut readonly: " + Venusaur.classe);
// Exemple d'us d'Interficie
let Squirtle = new PokemonInterfaceClasse();
Squirtle.Nom = "Squirtle";
Squirtle.HP = 100;
Squirtle.toString();
//# sourceMappingURL=prova3.js.map