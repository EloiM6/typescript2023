// Exemple de tipus tupla string
let arraytupla : [string, string] = ["Dormir està sobrevalorado", "Comer está sobrevalorado"];
console.log(arraytupla);
arraytupla.push("Hector se ha levantado a las 11");
console.log(arraytupla);
arraytupla.push("Arnau és sumiso");
console.log(arraytupla);

// Igual però amb numèrics
let arraytupla2 : [number, number] = [1 , 2];
arraytupla2.push(3);
console.log(arraytupla2);

// Exemple de tipus propi
// El simbol ? permet posar un paràmetre opcional

type jugador = {
    nom:string,
    cognom:string,
    alias:string,
    edat:number,
    correu?:string
}
let j1:jugador ={
  nom:"Vicioso1",
  cognom:"Soy muy guapo",
  alias:"mentiroso",
  edat:17//, correu:"jaja@vicio.org" -> És opcional i no cal posar-lo
};
console.log(j1);

// Exemple d'interficies
interface jugadorvideojocs {
    nom: string,
    cognom: string,
    alias: string,
    edat: number,
    correu?: string
    escriunomcognoms():void;
}
let jug:jugadorvideojocs={
    nom:"Jugador1",
    cognom: "Cognoms1",
    alias:"alias1",
    edat:10,
    escriunomcognoms(){
        console.log( jug.nom + " " + jug.cognom)
    }
}
console.log(jug);

// Exemple de record
// És un diccionari que l'indiquem el tipus
// El segon tipus em posat any perquè potser més d'un valor,
// però no és recomanable.
// Millor posar un tipat fort si és possible.
let objecterecord: Record<string,any> = {};
objecterecord.nom = "Jugador1";
objecterecord["cognom"]="cognoms2";
objecterecord["edat"]=18;

// Exemple tipus union
type videojocname = {
    nom:string
    Informacio: () => void;
}
type videojocpegy = {
    pegy:number
}

type videojoc = videojocname & videojocpegy;
let Videojoc:videojoc ={
    nom:"SuperMario",
    pegy:18,
    Informacio:function (){
        console.log("Prova");
    }
}
// Exemple de OR
type videojoc2 = videojocname | videojocpegy;
// No cal posar tots els tipus, però almenys un.
let Videojoc2:videojoc2 ={
    nom:"SuperMario",
    Informacio:function (){
        console.log("Prova");
    }
}
let Videojoc3:videojoc2 ={
    pegy:18
}
// Funcions
// Es poden declarar les variables com a opcionals o bé assignar-lis un valor

function suma(a:number,b:number):number {
    let c: number = a + b;
    return c;
}
let varsuma:number = suma(1,2);
console.log(varsuma)
function suma2(a:number,b?:number):number {
    if (b==null){
        console.log("És nan")
        return a;
    }
    let c: number = a + b;
    return c;
}
varsuma = suma2(1,2);
console.log(varsuma);
varsuma = suma2(1);

function suma3(a:number,b:number=0):number {
    if (b==null){
        console.log("És nan")
        return a;
    }
    let c: number = a + b;
    return c;
}

varsuma = suma3(1);
console.log(varsuma);
varsuma = suma3(1,1000);
console.log(varsuma);

// Una altra manera de declarar una funció és de la següent manera:
let altraFuncio = (a: string, b: string): string => {
    return a + b;
};
let var2 : Function =  altraFuncio;

console.log(altraFuncio("a","b"));
console.log(var2("c","v"));

// Tractament de números
let nombreEnter: number = 5;
let nombreDecimal: number = 5.55555

console.log(Number.isInteger(nombreEnter));
console.log(Number.isInteger(nombreDecimal));

let var3 : number = Number(nombreDecimal.toFixed(2));
console.log(var3);
