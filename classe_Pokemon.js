export class Pokemon {
    constructor(Nom, NumColeccio, HP, evoluciona) {
        this._Nom = Nom;
        this._NumColeccio = NumColeccio;
        this._HP = HP;
        this._evoluciona = evoluciona;
        this.classe = "Pokemon";
    }
    get evoluciona() {
        return this._evoluciona;
    }
    set evoluciona(value) {
        this._evoluciona = value;
    }
    get HP() {
        return this._HP;
    }
    set HP(value) {
        this._HP = value;
    }
    get NumColeccio() {
        return this._NumColeccio;
    }
    set NumColeccio(value) {
        this._NumColeccio = value;
    }
    get Nom() {
        return this._Nom;
    }
    set Nom(value) {
        this._Nom = value;
    }
    toString() {
        console.log("Nom: " + this._Nom + ", Número coleccio: " + this._NumColeccio + ", HP: " + this._HP + ", Evoluciona: " + this._evoluciona);
    }
}
//# sourceMappingURL=classe_Pokemon.js.map