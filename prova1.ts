console.log("Hola Hola");
// Declaració d'un boleà
let mybool:boolean=true;
console.log(mybool);
// Declaració d'un numeric
let num : number = 2;
// Declaració d'un string
let string1 : string = "Cadena";
let arrayclassic = [25, 'classic', true];
// Fixar el tipus l'array
let arraytipat : string[] =  ['formatge', 'atun','tomaquet','pollo'];

let cosa :any = "Hola";
cosa = 1;

let a:HTMLParagraphElement = document.getElementById('p1') as HTMLParagraphElement;
a.innerText= 'jajaj';

let a2:HTMLDivElement = document.getElementById('p2') as HTMLDivElement;
a2.innerText= 'jajaj2';

let boto : HTMLButtonElement = document.getElementById('boto') as HTMLButtonElement;
boto.addEventListener('click', getNomcomplet);

function getNomcomplet(event:Event):void {
    event.preventDefault();
    let inputnom : HTMLInputElement = document.getElementById('nom') as HTMLInputElement;
    let inputcognoms : HTMLInputElement = document.getElementById('cognoms') as HTMLInputElement;
    let nomcomplet : string = "Nom és: " + inputnom.value + " i els cognoms són: " + inputcognoms.value;
    let p3:HTMLParagraphElement = document.getElementById('p3') as HTMLParagraphElement;
    p3.innerText = nomcomplet;
    event.stopPropagation();
}