import {Pokemon} from "./classe_Pokemon.js";
export class Evolucio extends Pokemon{
    private _NomEvolucio : string="";
    get NomEvolucio(): string {
        return this._NomEvolucio;
    }

    set NomEvolucio(value: string) {
        this._NomEvolucio = value;
    }


}