console.log("Hola Hola");
// Declaració d'un boleà
let mybool = true;
console.log(mybool);
// Declaració d'un numeric
let num = 2;
// Declaració d'un string
let string1 = "Cadena";
let arrayclassic = [25, 'classic', true];
// Fixar el tipus l'array
let arraytipat = ['formatge', 'atun', 'tomaquet', 'pollo'];
let cosa = "Hola";
cosa = 1;
let a = document.getElementById('p1');
a.innerText = 'jajaj';
let a2 = document.getElementById('p2');
a2.innerText = 'jajaj2';
let boto = document.getElementById('boto');
boto.addEventListener('click', getNomcomplet);
function getNomcomplet(event) {
    event.preventDefault();
    let inputnom = document.getElementById('nom');
    let inputcognoms = document.getElementById('cognoms');
    let nomcomplet = "Nom és: " + inputnom.value + " i els cognoms són: " + inputcognoms.value;
    let p3 = document.getElementById('p3');
    p3.innerText = nomcomplet;
    event.stopPropagation();
}
//# sourceMappingURL=prova1.js.map