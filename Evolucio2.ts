import {Pokemon} from "./classe_Pokemon.js";

export class Evolucio2 extends Pokemon {
    private _NomEvolucio : string ="";
    readonly classe:string;
    constructor(Nom:string,NumColeccio:number,HP:number, evoluciona:boolean, Nomevo:string) {
        super(Nom,NumColeccio,HP, evoluciona);
        this._NomEvolucio = Nomevo;
        this.classe="Evolucio2";
    }
    get NomEvolucio(): string {
        return this._NomEvolucio;
    }

    set NomEvolucio(value: string) {
        this._NomEvolucio = value;
    }

    // Sobreescriptura del mètode
    toString(){
        // Crida del mètode pare
        //super.toString();
        console.log("Nom: " + this.Nom + ", Número coleccio: " + this.NumColeccio + ", HP: " + this.HP + ", Evoluciona: " + this.evoluciona + ", Nom Evolucio: " + this.NomEvolucio);
    }
}