export class Pokemon{
    // Creació d'atributs
    // Modificadors dels atributs. Per defecte públic.
    // Altres valors: Private, Protected
    // Igual que en java
    // Altres valors: static i readonly
    private _Nom:String;
    private _NumColeccio : number;
    private _HP : number;
    private _evoluciona : boolean;
    readonly classe:string;

    constructor(Nom : string,  NumColeccio: number, HP : number, evoluciona:boolean) {
        this._Nom = Nom;
        this._NumColeccio = NumColeccio;
        this._HP = HP;
        this._evoluciona = evoluciona;
        this.classe = "Pokemon";
    }
    get evoluciona(): boolean {
        return this._evoluciona;
    }

    set evoluciona(value: boolean) {
        this._evoluciona = value;
    }
    get HP(): number {
        return this._HP;
    }

    set HP(value: number) {
        this._HP = value;
    }
    get NumColeccio(): number {
        return this._NumColeccio;
    }

    set NumColeccio(value: number) {
        this._NumColeccio = value;
    }
    get Nom(): String {
        return this._Nom;
    }
    set Nom(value: String) {
        this._Nom = value;
    }
    toString(){
        console.log("Nom: " + this._Nom + ", Número coleccio: " + this._NumColeccio + ", HP: " + this._HP + ", Evoluciona: " + this._evoluciona);
    }
}