import { Pokemon } from "./classe_Pokemon.js";
export class Evolucio extends Pokemon {
    constructor() {
        super(...arguments);
        this._NomEvolucio = "";
    }
    get NomEvolucio() {
        return this._NomEvolucio;
    }
    set NomEvolucio(value) {
        this._NomEvolucio = value;
    }
}
//# sourceMappingURL=Evolucio.js.map