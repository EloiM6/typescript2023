import { Pokemon } from "./classe_Pokemon.js";
export class Evolucio2 extends Pokemon {
    constructor(Nom, NumColeccio, HP, evoluciona, Nomevo) {
        super(Nom, NumColeccio, HP, evoluciona);
        this._NomEvolucio = "";
        this._NomEvolucio = Nomevo;
        this.classe = "Evolucio2";
    }
    get NomEvolucio() {
        return this._NomEvolucio;
    }
    set NomEvolucio(value) {
        this._NomEvolucio = value;
    }
    // Sobreescriptura del mètode
    toString() {
        // Crida del mètode pare
        //super.toString();
        console.log("Nom: " + this.Nom + ", Número coleccio: " + this.NumColeccio + ", HP: " + this.HP + ", Evoluciona: " + this.evoluciona + ", Nom Evolucio: " + this.NomEvolucio);
    }
}
//# sourceMappingURL=Evolucio2.js.map