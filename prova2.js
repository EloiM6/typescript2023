"use strict";
// Exemple de tipus tupla string
let arraytupla = ["Dormir està sobrevalorado", "Comer está sobrevalorado"];
console.log(arraytupla);
arraytupla.push("Hector se ha levantado a las 11");
console.log(arraytupla);
arraytupla.push("Arnau és sumiso");
console.log(arraytupla);
// Igual però amb numèrics
let arraytupla2 = [1, 2];
arraytupla2.push(3);
console.log(arraytupla2);
let j1 = {
    nom: "Vicioso1",
    cognom: "Soy muy guapo",
    alias: "mentiroso",
    edat: 17 //, correu:"jaja@vicio.org" -> És opcional i no cal posar-lo
};
console.log(j1);
let jug = {
    nom: "Jugador1",
    cognom: "Cognoms1",
    alias: "alias1",
    edat: 10,
    escriunomcognoms() {
        console.log(jug.nom + " " + jug.cognom);
    }
};
console.log(jug);
// Exemple de record
// És un diccionari que l'indiquem el tipus
// El segon tipus em posat any perquè potser més d'un valor,
// però no és recomanable.
// Millor posar un tipat fort si és possible.
let objecterecord = {};
objecterecord.nom = "Jugador1";
objecterecord["cognom"] = "cognoms2";
objecterecord["edat"] = 18;
let Videojoc = {
    nom: "SuperMario",
    pegy: 18,
    Informacio: function () {
        console.log("Prova");
    }
};
// No cal posar tots els tipus, però almenys un.
let Videojoc2 = {
    nom: "SuperMario",
    Informacio: function () {
        console.log("Prova");
    }
};
let Videojoc3 = {
    pegy: 18
};
// Funcions
// Es poden declarar les variables com a opcionals o bé assignar-lis un valor
function suma(a, b) {
    let c = a + b;
    return c;
}
let varsuma = suma(1, 2);
console.log(varsuma);
function suma2(a, b) {
    if (b == null) {
        console.log("És nan");
        return a;
    }
    let c = a + b;
    return c;
}
varsuma = suma2(1, 2);
console.log(varsuma);
varsuma = suma2(1);
function suma3(a, b = 0) {
    if (b == null) {
        console.log("És nan");
        return a;
    }
    let c = a + b;
    return c;
}
varsuma = suma3(1);
console.log(varsuma);
varsuma = suma3(1, 1000);
console.log(varsuma);
// Una altra manera de declarar una funció és de la següent manera:
let altraFuncio = (a, b) => {
    return a + b;
};
let var2 = altraFuncio;
console.log(altraFuncio("a", "b"));
console.log(var2("c", "v"));
// Tractament de números
let nombreEnter = 5;
let nombreDecimal = 5.55555;
console.log(Number.isInteger(nombreEnter));
console.log(Number.isInteger(nombreDecimal));
let var3 = Number(nombreDecimal.toFixed(2));
console.log(var3);
//# sourceMappingURL=prova2.js.map